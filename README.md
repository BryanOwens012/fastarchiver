# FastArchiver

**fast-archiver.sh** is a simple script to archive webpages using the command line.

The script takes one argument, a URL, as input. When run, it requests the Wayback Machine to archive that URL, by downloading and then immediately deleting the archived page source.

If the URL contains special characters (such as "!"), wrap the URL with single quotes.

The script respects the presence of `robots.txt`.

No setup necessary.