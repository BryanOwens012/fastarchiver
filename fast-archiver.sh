#!/bin/bash
                                                                                          
wa() {                          
    to_save="$1"
    wa_to_save="https://web.archive.org/save/$to_save" # webarchive URL to save                               
    
    res=$(curl "$wa_to_save" >/dev/null 2>&1) # don't print any output or errors

    if [ $((res)) != "0" ]; then # expand the response to an integer
        echo "Failed to archive $to_save"
    else
        echo "Archived to Wayback Machine successfully: $to_save"
    fi
}

wa "$1"